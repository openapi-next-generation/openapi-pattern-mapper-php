<?php

namespace OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern;

abstract class AbstractContainerPattern extends PropertyPattern
{
    /**
     * @var PropertyPattern
     */
    protected $contentProperty;


    public function getContentProperty(): ?PropertyPattern
    {
        return $this->contentProperty;
    }

    public function setContentProperty(PropertyPattern $contentProperty): void
    {
        $this->contentProperty = $contentProperty;
    }
}