<?php

namespace OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern;

class EntityPattern extends PropertyPattern
{
    /**
     * @var string
     */
    protected $className;
    /**
     * @var array
     */
    protected $properties = [];


    public function setClassName(string $className): void
    {
        $this->className = $className;
    }

    public function getClassName(): string
    {
        return $this->className;
    }

    public function addProperty(PropertyPattern $property): void
    {
        $this->properties[] = $property;
    }

    public function getProperties(): array
    {
        return $this->properties;
    }
}
