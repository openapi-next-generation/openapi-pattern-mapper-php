<?php

namespace OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern;

class PropertyPattern
{
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $type;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var bool
     */
    protected $required = false;
    /**
     * @var bool
     */
    protected $nullable = false;
    /**
     * @var mixed
     */
    protected $default;


    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function setRequired(bool $required): void
    {
        $this->required = $required;
    }

    public function isNullable(): bool
    {
        return $this->nullable;
    }

    public function setNullable(bool $nullable): void
    {
        $this->nullable = $nullable;
    }

    public function setDefault($default): void
    {
        $this->default = $default;
    }

    public function getDefault()
    {
        return $this->default;
    }

    public function getUpperCamelCaseName(): ?string
    {
        return str_replace(['-', '_', '.', ':'], '', ucwords($this->name, '-_.:'));
    }

    public function getLowerCamelCaseName(): ?string
    {
        return lcfirst($this->getUpperCamelCaseName());
    }
}
