<?php

namespace OpenapiNextGeneration\OpenapiPatternMapperPhp;

use OpenapiNextGeneration\GenerationHelperPhp\SpecificationModelsHelper;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\ArrayPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\AssocPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\EntityPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\PropertyPattern;

/**
 * Class to map specification file to pattern objects
 */
class PatternMapper
{
    public function buildPatterns(array $specification)
    {
        $patterns = [];
        foreach (SpecificationModelsHelper::readModels($specification) as $name => $specification) {
            $patterns[] = $this->createSpecificationPattern($name, $specification, true);
        }

        return $patterns;
    }

    /**
     * Adds each external EntityPattern to the list of patterns
     */
    public function copyExternalEntites(array $patterns): array
    {
        $resultPatterns = $this->findEntityPatternsRec($patterns);
        foreach ($patterns as $pattern) {
            if (!$pattern instanceof EntityPattern) {
                $resultPatterns[] = $pattern;
            }
        }
        return array_values($resultPatterns);
    }

    protected function findEntityPatternsRec(array $patterns): array
    {
        $resultPatterns = [];
        foreach ($patterns as $pattern) {
            if ($pattern instanceof EntityPattern) {
                $resultPatterns[$pattern->getClassName()] = $pattern;
                foreach ($this->findEntityPatternsRec($pattern->getProperties()) as $name => $property) {
                    if (!isset($resultPatterns[$name])) {
                        $resultPatterns[$name] = $property;
                    }
                }
            }
        }
        return $resultPatterns;
    }

    /**
     * Maps openapi model to pattern object
     *
     * @param string $name
     * @param array $specification
     * @param bool $isEntity
     * @return ArrayPattern|AssocPattern|EntityPattern|PropertyPattern
     */
    protected function createSpecificationPattern(string $name, array $specification, bool $isEntity)
    {
        if (!isset($specification['type'])) {
            $specification['type'] = 'object';
        }
        if ($specification['type'] === 'object') {
            if ($isEntity) {
                $pattern = new EntityPattern();
                $pattern->setName($name);
                if (isset($specification['$ref'])) {
                    $className = substr($specification['$ref'], strrpos($specification['$ref'], '/') + 1);
                    $pattern->setClassName($className);
                } else {
                    $pattern->setClassName($name);
                }
                foreach ($specification['properties'] ?? [] as $propertyName => $propertyDef) {
                    $propertyPattern = $this->createSpecificationPattern(
                        $propertyName,
                        $propertyDef,
                        isset($propertyDef['$ref'])
                    );
                    $pattern->addProperty($propertyPattern);
                }
                if (isset($specification['required'])) {
                    /* @var $property PropertyPattern */
                    foreach ($pattern->getProperties() as $property) {
                        if (in_array($property->getName(), $specification['required'], true)) {
                            $property->setRequired(true);
                        }
                    }
                }
            } else {
                $pattern = new AssocPattern();
                $pattern->setName($name);
                foreach ($specification['properties'] ?? [] as $propertyName => $propertyDef) {
                    $propertyPattern = $this->createSpecificationPattern($propertyName, $propertyDef,
                        isset($propertyDef['$ref']));
                    $pattern->setContentProperty($propertyPattern);
                    break;  //pick first property of assoc as content type
                }
            }
        } elseif ($specification['type'] === 'array') {
            $pattern = new ArrayPattern();
            $itemPattern = $this->createSpecificationPattern(
                'items',
                $specification['items'],
                isset($specification['items']['$ref'])
            );
            $pattern->setContentProperty($itemPattern);
            $pattern->setName($name);
        } else {
            $pattern = new PropertyPattern();
            $pattern->setName($name);
        }
        $pattern->setType($specification['type']);
        if (isset($specification['nullable'])) {
            $pattern->setNullable($specification['nullable']);
        }
        if (isset($specification['description'])) {
            $pattern->setDescription($specification['description']);
        }
        if (isset($specification['default'])) {
            $pattern->setDefault($specification['default']);
        }
        return $pattern;
    }
}